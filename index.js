const { dockStart } = require("@nlpjs/basic");

async function main() {
  await dockStart();
}

main().catch(console.error);
