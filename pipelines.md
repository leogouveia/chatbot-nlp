# Pipelines

## console.hear

eq input.message "sair"
jne sendMessage
console.exit
label sendMessage
nlp.process input.message
.say input.answer

## main

nlp.train
console.say "Olá, sou sua assistente! Como posso ajudar?"
